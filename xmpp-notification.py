#!/usr/bin/env python3

import slixmpp
import sys,os
import ssl

class SendMsgBot(slixmpp.ClientXMPP):

    def __init__(self, jid, password, recipient, msg):
        super().__init__(jid, password)

        self.recipient = recipient
        self.msg = msg

        # Service Discovery
        self.register_plugin('xep_0030')
        # XMPP Ping
        self.register_plugin('xep_0199')

        self.add_event_handler('session_start', self.start)

    def start(self, event):
        self.get_roster()
        self.send_message(mto=self.recipient, mbody=self.msg, mtype='chat')
        self.disconnect()

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage:", sys.argv[0], "RECIPIENT MESSAGE", file=sys.stderr)
        sys.exit(1)
    xmpp = SendMsgBot(os.environ["XMPP_USER"], os.environ["XMPP_PASSWORD"], sys.argv[1], sys.argv[2])
    xmpp.connect()
    xmpp.process(forever=False)
